var express = require('express');
var router = express.Router();

var authenticateController=require('./../controllers/authenticate-controller');
var registerController=require('./../controllers/register-controller');

/* GET home page. */
router.get('/', function(req, res, next) {
  
  res.render('index');

});

//register
router.post('/api/register', registerController.register)
router.get('/api/register', function (req, res) {
  res.render("index", { title: "Registr page" });
});
//auth
router.post('/api/auth', authenticateController.authenticate)
router.get('/api/auth', function (req, res) {
  res.render("index", { title: "Login page" });
});

module.exports = router;
