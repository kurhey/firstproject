'use strict';
var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var logoutController=require('./../controllers/logout-controller');
var coinController = require('./../controllers/coins-controller');


router.all('*', function(req, res, next){
    try{
      var token = localStorage.getItem('auth_token');
      var user = get_user(token, req.app.get('secret'))
    console.log(token)
    }catch(e){
      
    }
    req.data = user;
    next();
});

// router.get('/', function(req, res, next){
//     var user = req.data;
//     console.log(user);
//     if(user){res.render('users' ,  {user : user}); next()}
//     else res.send('<h1>Time out</h1>');
//   });

  router.get('/logout', function(req, res, next){
        logoutController.logout(req, res, next);
  });

  
  router.all('/', coinController.getCoins);


  function get_token(user, secret) {
    let token = jwt.sign(user, secret, { expiresIn: 200000 });
    return token;
}

function get_user(token, secret) {
    try {
        var user = jwt.verify(token, secret);
        return user;
    } catch(e)
    {
        return null;
    }
}

module.exports = {
    router: router,
    get_token: get_token,
    get_user: get_user
};
