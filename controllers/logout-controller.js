

module.exports.logout=function(req, res, next){
    localStorage.clear();
    res.redirect('/');
};
