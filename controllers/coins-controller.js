
const rp = require('request-promise')
var multiparty = require('multiparty');

module.exports.getCoins = function (req, res) {
    let id = "bitcoin";
    let uri = 'https://api.coingecko.com/api/v3/coins/list'
    var user = req.data;
    var form = new multiparty.Form();
    if (user) {
        rp({
            uri: uri,
            json: true
        }).then((coins) => {
            form.parse(req, function (error, fields, files) {
                if (fields) {
                    let name = Object.keys(fields);
                    id = name[0];
                }
                let uriCoin = 'https://api.coingecko.com/api/v3/coins/' + id;
                rp({
                    uri: uriCoin,
                    json: true
                }).then((coin) => {
                    res.render('users', {
                        user : user,
                        coins: coins,
                        coin: coin
                    })
                })
                    .catch((err) => {
                        console.log(err)
                    })
            })
        })
            .catch((err) => {
                console.log(err)
            })
    }
    else {
        res.render("index", {info: "Time out, Please login" });
    }
}